﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rate_favourite_food
{
    class Program
    {
        static void Main(string[] args)
        {
            var l = 1;
            do
            {
                Console.Clear();
                var food = new SortedDictionary<string, string>();
                var rating = new Dictionary<string, int>();

                Console.WriteLine("Write your 5 favourite foods.");
                food.Add("one", Console.ReadLine());
                food.Add("two", Console.ReadLine());
                food.Add("three", Console.ReadLine());
                food.Add("four", Console.ReadLine());
                food.Add("five", Console.ReadLine());

                Console.Clear();

                Console.WriteLine($"These are the Foods you chose. {food["one"]}, {food["two"]}, {food["three"]}, {food["four"]}, {food["five"]} ");
                Console.Clear();

                Console.WriteLine($"Rate {food["one"]} out of 10.");
                rating.Add("one", int.Parse(Console.ReadLine()));

                Console.WriteLine($"Rate {food["two"]} out of 10.");
                rating.Add("two", int.Parse(Console.ReadLine()));

                Console.WriteLine($"Rate {food["three"]} out of 10.");
                rating.Add("three", int.Parse(Console.ReadLine()));

                Console.WriteLine($"Rate {food["four"]} out of 10.");
                rating.Add("four", int.Parse(Console.ReadLine()));

                Console.WriteLine($"Rate {food["five"]} out of 10.");
                rating.Add("five", int.Parse(Console.ReadLine()));

                Console.Clear();

                Console.WriteLine($"You gave {food["one"]} a rating of {rating["one"]} out of 10");
                Console.WriteLine($"You gave {food["two"]} a rating of {rating["two"]} out of 10");
                Console.WriteLine($"You gave {food["three"]} a rating of {rating["three"]} out of 10");
                Console.WriteLine($"You gave {food["four"]} a rating of {rating["four"]} out of 10");
                Console.WriteLine($"You gave {food["five"]} a rating of {rating["five"]} out of 10");
                Console.WriteLine();
                Console.WriteLine("Press Enter to continue.");
                Console.ReadLine();
                Console.Clear();

                //I am having trouble figuring out how to sort dictionary so i have not completed this section//

                /*
                Console.WriteLine("Do you want to sort by most favourite food?");
                var prompt = Console.ReadLine();

                var list = rating.OrderBy(Value => Value.Key);



                if(prompt == "Y")
                {
                 
                    Console.WriteLine($"{rating.OrderBy(Value => Value.Key)}");
                }

                if (prompt == "y")
                {

                    Console.WriteLine($"{rating.OrderBy(Key => Key.Value)}");
                }



                Console.ReadLine();
                Console.Clear();
                */
                Console.WriteLine("Do you want to Rate the food again? Y or N");
                
                var answer = Console.ReadLine();
                if (answer == "Y")
                {
                    l = 1;
                }
                if (answer == "y")
                {
                    l = 1;
                }

                if (answer == "N")
                {
                    l = 0;
                }
                if (answer == "n")
                {
                    l = 0;
                }
            } while (l > 0);
        }
    }
}
